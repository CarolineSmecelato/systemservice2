/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import entidade.ContasPagar;
import entidade.Pessoa;
import facade.ContasPagarFacade;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author carol
 */
@Named
@SessionScoped
public class ContaPagarControle implements Serializable {

    @Inject
    private ContasPagarFacade contasPagarFacade;
    private ContasPagar contasPagar;
    
    private Pessoa pessoa;

    public void novo(){
        contasPagar = new ContasPagar();
    }

    public ContasPagarFacade getContasPagarFacade() {
        return contasPagarFacade;
    }

    public void setContasPagarFacade(ContasPagarFacade contasPagarFacade) {
        this.contasPagarFacade = contasPagarFacade;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }
    
    
    
    public void excluir(ContasPagar e){
        contasPagarFacade.excluir(e);
    }
    
    public void editar(ContasPagar e){
        if(e.isPago()){
            e.setPago(Boolean.FALSE);
            e.setDataPagamento(null);
        } else {
            e.setPago(Boolean.TRUE);
            e.setDataPagamento(new Date());
        }
        contasPagar = e;
        
        this.salvar();
    }
    
    public void salvar() {
        contasPagarFacade.salvar(contasPagar);
    }

    public List<ContasPagar> listaTodos() {
        return contasPagarFacade.listaPorCliente(pessoa);
    }

    public ContasPagar getContasPagar() {
        return contasPagar;
    }

    public void setContasPagar(ContasPagar contasPagar) {
        this.contasPagar = contasPagar;
    }
    
    public String pegaStatus(ContasPagar c){
        if(c.isPago()){
            return "Estornar";
        } else {
            return "Pagar";
        }
    }   

}
