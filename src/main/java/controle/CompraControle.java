/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import converter.ConverterGenerico;
import converter.MoneyConverter;
import entidade.ItensProduto;
import entidade.Compra;
import entidade.Pessoa;
import entidade.Produto;
import facade.CompraFacade;
import facade.PessoaFacade;
import facade.ProdutoFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;

/**
 *
 * @author carol
 */
@Named
@ViewAccessScoped
public class CompraControle implements Serializable {

    @Inject
    private CompraFacade compraFacade;
    private Compra compra;
    private ItensProduto itensProduto;
    @Inject
    private PessoaFacade pessoaFacade;
    @Inject
    private ProdutoFacade produtoFacade;
    private ConverterGenerico converterPessoa;
    private MoneyConverter moneyConverter;
    private ConverterGenerico converterProduto;

    public ConverterGenerico getConverterProduto() {
        if (converterProduto == null) {
            converterProduto = new ConverterGenerico(produtoFacade);
        }
        return converterProduto;
    }

    public void setConverterProduto(ConverterGenerico converterProduto) {
        this.converterProduto = converterProduto;
    }

    public void addItemProduto() {
        Double estoque = itensProduto.getProduto().getEstoque();
        ItensProduto itemTemp = null;
        for (ItensProduto it : compra.getItensProdutos()) {
            if (it.getProduto().equals(itensProduto.getProduto())) {
                estoque = estoque + it.getQuantidade();
                itemTemp = it;
            }
        }        
        if (itensProduto.getQuantidade() < 1) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(
                            FacesMessage.SEVERITY_ERROR,
                            "Impossível comprar!",
                            +itensProduto.getQuantidade()
                            + " Menor que 1"));
        } else {
            if (itemTemp != null) {
                itemTemp.setQuantidade(itemTemp.getQuantidade() + itensProduto.getQuantidade());
            } else {
                itensProduto.setCompra(compra);
                compra.getItensProdutos().add(itensProduto);
            }
            itensProduto = new ItensProduto();
        }

    }

    public void removerItemProduto(ItensProduto it) {
        compra.getItensProdutos().remove(it);
    }



    public MoneyConverter getMoneyConverter() {
        if (moneyConverter == null) {
            moneyConverter = new MoneyConverter();
        }
        return moneyConverter;
    }

    public void setMoneyConverter(MoneyConverter moneyConverter) {
        this.moneyConverter = moneyConverter;
    }



    public void setaValorProduto() {
        itensProduto.setValor(itensProduto.getProduto().getValor());
    }

    public ItensProduto getItensProduto() {
        return itensProduto;
    }

    public void setItensProduto(ItensProduto itensProduto) {
        this.itensProduto = itensProduto;
    }


    public List<Produto> listaFiltrandoProduto(String parte) {
        return produtoFacade.listaFiltrando(parte, "nome");
    }


    public List<Pessoa> listaFiltrando(String parte) {
        return pessoaFacade.listaFiltrando(parte, "nome");
    }

    public ConverterGenerico getConverterPessoa() {
        if (converterPessoa == null) {
            converterPessoa = new ConverterGenerico(pessoaFacade);
        }
        return converterPessoa;
    }

    public void setConverterPessoa(ConverterGenerico converterPessoa) {
        this.converterPessoa = converterPessoa;
    }

    public void novo() {
        compra = new Compra();
        itensProduto = new ItensProduto();
    }

    public void excluir(Compra e) {
        compraFacade.excluir(e);
    }

    public void editar(Compra e) {
        this.compra = e;
    }

    public void salvar() {
        compraFacade.salvar(compra);
    }

    public List<Compra> listaTodos() {
        return compraFacade.listaTodos();
    }

    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

}
