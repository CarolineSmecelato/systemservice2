/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import converter.ConverterGenerico;
import converter.MoneyConverter;
import entidade.ContasReceber;
import entidade.ItensProduto;
import entidade.Venda;
import entidade.Pessoa;
import entidade.Produto;
import facade.VendaFacade;
import facade.PessoaFacade;
import facade.ProdutoFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;

/**
 *
 * @author carol
 */
@Named
@ViewAccessScoped
public class VendaControle implements Serializable {

    @Inject
    private VendaFacade vendaFacade;
    private Venda venda;
    private ItensProduto itensProduto;
    @Inject
    private PessoaFacade pessoaFacade;
    @Inject
    private ProdutoFacade produtoFacade;
    private ConverterGenerico converterPessoa;
    private ConverterGenerico converterServico;
    private MoneyConverter moneyConverter;
    private ConverterGenerico converterProduto;
    private Integer numParcela;
    private Date dtVencimento;

    public Integer getNumParcela() {
        return numParcela;
    }

    public void setNumParcela(Integer numParcela) {
        this.numParcela = numParcela;
    }

    public Date getDtVencimento() {
        return dtVencimento;
    }

    public void setDtVencimento(Date dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    public ConverterGenerico getConverterProduto() {
        if (converterProduto == null) {
            converterProduto = new ConverterGenerico(produtoFacade);
        }
        return converterProduto;
    }

    public void setConverterProduto(ConverterGenerico converterProduto) {
        this.converterProduto = converterProduto;
    }

    public void addItemProduto() {
        Double estoque = itensProduto.getProduto().getEstoque();
        ItensProduto itemTemp = null;
        for (ItensProduto it : venda.getItensProdutos()) {
            if (it.getProduto().equals(itensProduto.getProduto())) {
                estoque = estoque - it.getQuantidade();
                itemTemp = it;
            }
        }
        if (estoque < itensProduto.getQuantidade()) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(
                            FacesMessage.SEVERITY_ERROR,
                            "Estoque insuficiente!",
                            "Restam apenas "
                            + estoque
                            + " unidades."));
        } else {
            if (itemTemp != null) {
                itemTemp.setQuantidade(itemTemp.getQuantidade() + itensProduto.getQuantidade());
            } else {
                itensProduto.setVenda(venda);
                venda.getItensProdutos().add(itensProduto);
            }
            itensProduto = new ItensProduto();
        }

    }

    public void removerItemProduto(ItensProduto it) {
        venda.getItensProdutos().remove(it);
    }

    public MoneyConverter getMoneyConverter() {
        if (moneyConverter == null) {
            moneyConverter = new MoneyConverter();
        }
        return moneyConverter;
    }

    public void setMoneyConverter(MoneyConverter moneyConverter) {
        this.moneyConverter = moneyConverter;
    }

    public void setaValorProduto() {
        itensProduto.setValor(itensProduto.getProduto().getValor());
    }

    public ItensProduto getItensProduto() {
        return itensProduto;
    }

    public void setItensProduto(ItensProduto itensProduto) {
        this.itensProduto = itensProduto;
    }


    public List<Produto> listaFiltrandoProduto(String parte) {
        return produtoFacade.listaFiltrando(parte, "nome");
    }



    public void setConverterServico(ConverterGenerico converterServico) {
        this.converterServico = converterServico;
    }

    public List<Pessoa> listaFiltrando(String parte) {
        return pessoaFacade.listaFiltrando(parte, "nome");
    }

    public ConverterGenerico getConverterPessoa() {
        if (converterPessoa == null) {
            converterPessoa = new ConverterGenerico(pessoaFacade);
        }
        return converterPessoa;
    }

    public void setConverterPessoa(ConverterGenerico converterPessoa) {
        this.converterPessoa = converterPessoa;
    }

    public void novo() {
        venda = new Venda();
        itensProduto = new ItensProduto();
    }

    public void excluir(Venda e) {
        vendaFacade.excluir(e);
    }

    public void editar(Venda e) {
        this.venda = e;
    }

    public void salvar() {
        vendaFacade.salvar(venda);
    }

    public List<Venda> listaTodos() {
        return vendaFacade.listaTodos();
    }

    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    public void geraParcela() {
        venda.setContasRecebers(new ArrayList<ContasReceber>());
        Date dtparcela = dtVencimento;
        for (int i = 1; i <= numParcela; i++) {
            ContasReceber cr = new ContasReceber();
            cr.setDataLancamento(new Date());
            cr.setNumeroParcela(i);
            cr.setVenda(venda);
            cr.setPessoa(venda.getPessoa());
            cr.setValor(venda.getValorTotal() / numParcela);
            cr.setDataVencimento(dtparcela);
            cr.setPago(Boolean.FALSE);
            
            Calendar cal = Calendar.getInstance();
            cal.setTime(dtparcela);
            cal.add(Calendar.MONTH, 1);
            dtparcela = cal.getTime();
            
            venda.getContasRecebers().add(cr);
        }
    }

}
