/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import entidade.ContasReceber;
import entidade.Pessoa;
import facade.ContasReceberFacade;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author carol
 */
@Named
@SessionScoped
public class ContaReceberControle implements Serializable {

    @Inject
    private ContasReceberFacade contasReceberFacade;
    private ContasReceber contasReceber;
    
    private Pessoa pessoa;

    public void novo(){
        contasReceber = new ContasReceber();
    }

    public ContasReceberFacade getContasReceberFacade() {
        return contasReceberFacade;
    }

    public void setContasReceberFacade(ContasReceberFacade contasReceberFacade) {
        this.contasReceberFacade = contasReceberFacade;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }
    
    
    
    public void excluir(ContasReceber e){
        contasReceberFacade.excluir(e);
    }
    
    public void editar(ContasReceber e){
        if(e.isPago()){
            e.setPago(Boolean.FALSE);
            e.setDataPagamento(null);
        } else {
            e.setPago(Boolean.TRUE);
            e.setDataPagamento(new Date());
        }
        contasReceber = e;
        
        this.salvar();
    }
    
    public void salvar() {
        contasReceberFacade.salvar(contasReceber);
    }

    public List<ContasReceber> listaTodos() {
        return contasReceberFacade.listaPorCliente(pessoa);
    }

    public ContasReceber getContasReceber() {
        return contasReceber;
    }

    public void setContasReceber(ContasReceber contasReceber) {
        this.contasReceber = contasReceber;
    }
    
    public String pegaStatus(ContasReceber c){
        if(c.isPago()){
            return "Estornar";
        } else {
            return "Pagar";
        }
    }   

}
