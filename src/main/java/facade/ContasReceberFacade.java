package facade;

import entidade.ContasReceber;
import entidade.Pessoa;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import util.Transacional;

@Transacional
public class ContasReceberFacade extends AbstractFacade<ContasReceber>{

    @Inject
    private EntityManager em;

    public ContasReceberFacade() {
        super(ContasReceber.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public List<ContasReceber> listaPorCliente(Pessoa p){
        Query q = getEntityManager().createQuery("from ContasReceber WHERE venda.pessoa = :pessoa");
        q.setParameter("pessoa", p);
        return q.getResultList();
    }
}
