package facade;

import entidade.ContasPagar;
import entidade.Pessoa;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import util.Transacional;

@Transacional
public class ContasPagarFacade extends AbstractFacade<ContasPagar>{

    @Inject
    private EntityManager em;

    public ContasPagarFacade() {
        super(ContasPagar.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public List<ContasPagar> listaPorCliente(Pessoa p){
        Query q = getEntityManager().createQuery("from ContasPagar WHERE compra.pessoa = :pessoa");
        q.setParameter("pessoa", p);
        return q.getResultList();
    }
}
