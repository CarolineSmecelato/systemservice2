package facade;

import entidade.Compra;
import entidade.ItensProduto;
import entidade.Produto;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import util.Transacional;

@Transacional
public class CompraFacade extends AbstractFacade<Compra>{

    @Inject
    private EntityManager em;

    public CompraFacade() {
        super(Compra.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public void salvar(Compra c) {        
        super.salvar(c); 
        aumentaEstoque(c);
    }

    private void aumentaEstoque(Compra c) {
        for(ItensProduto it : c.getItensProdutos()){
            Produto p = it.getProduto();
            p.setEstoque(p.getEstoque() + it.getQuantidade());
            em.merge(p);
        }
    }
    
    
    
    
}
