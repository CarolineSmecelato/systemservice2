package facade;

import entidade.ItensProduto;
import entidade.Venda;
import entidade.Produto;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import util.Transacional;

@Transacional
public class VendaFacade extends AbstractFacade<Venda>{

    @Inject
    private EntityManager em;

    public VendaFacade() {
        super(Venda.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public void salvar(Venda os) {        
        super.salvar(os); 
        baixaEstoque(os);
    }

    private void baixaEstoque(Venda os) {
        for(ItensProduto it : os.getItensProdutos()){
            Produto p = it.getProduto();
            p.setEstoque(p.getEstoque() - it.getQuantidade());
            em.merge(p);
        }
    }
    
    
    
    
}
